<?php $i = 0; ?>
@if($table == 'users')
    <td class="flex-center">
        <div>
            <div><?php echo '{{$d->name}}'; ?></div>
            <div class="sub-text-1"><?php echo '{{$d->email}}'; ?></div>
        </div>
    </td>
    <td class="uppercase sub-text-1">
        <?php echo '{{$d->type}}'; ?>
    </td>
@else
    @foreach ($columns as $c)
        @if ($i > 0)
        <td class="uppercase sub-text-1" ><?php echo '{{$d->' . $c . '}}'; ?></td>
        @else
            <td >
                    <?php echo '{{$d->' . $c . '}}'; ?>
            </td>
        @endif
        <?php $i++; ?>
    @endforeach
@endif